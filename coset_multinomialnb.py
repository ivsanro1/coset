#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import re
import csv
import nltk
from nltk.tokenize import RegexpTokenizer
from sklearn.feature_extraction.text import CountVectorizer




# regular expressions used to clean up the tweet data
http_re = re.compile(r'http://[^\s]*|https://[^\s]*')
remove_ellipsis_re = re.compile(r'\.\.\.|…')
at_sign_re = re.compile(r'\@\S+')
punct_re = re.compile(r"[\"'\[\],.:;()\-&!]")
price_re = re.compile(r"\d+\.\d\d")
number_re = re.compile(r"\d+")
unicode_re = re.compile(r"\\\S+")

# converts to lower case and clean up the text

def normalize_tweet(tweet):
    t = tweet.lower()
    t = re.sub(http_re, 'ENLACE', t)
    t = re.sub('\n|\t', ' ', t)

    t = re.sub('á', 'a', t)
    t = re.sub('é', 'e', t)
    t = re.sub('í', 'i', t)
    t = re.sub('ó', 'o', t)
    t = re.sub('ú', 'u', t)
    t = re.sub(r'[^\x00-\x7F]+','', t)

    t = re.sub(''':|;|'|"|\?|¿|¡|!|$|%|/|\(|\)|\-|\.|,|º|…|#''', '', t)
    t = re.sub('\s\d+\s', 'NUM', t)

    t = re.sub('\s\s+', ' ', t)

    tokenizer = RegexpTokenizer('[a-z0-9_ñ]+|[A-ZÑ]+')
    t = tokenizer.tokenize(t)
    t = ' '.join(t)

    return t

def tweet_features(tweet):
    features = {}

    text = normalize_tweet(tweet['text'])

    for bigrams in nltk.bigrams(text.split(' ')):
        features['contains(%s)' % ','.join(bigrams)] = True

    return features




path_train = "train/coset-train.csv"
path_dev = "train/coset-dev.csv"


data_train = []
data_dev = []


with open(path_train, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_train.append({'id':row[0],'text':row[1],'topic':row[2]})

with open(path_dev, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_dev.append({'id':row[0],'text':row[1],'topic':row[2]})



data_train.pop(0)
data_dev.pop(0)

data = data_train+data_dev

text_documents = [normalize_tweet(d['text']) for d in data]


count_vect = CountVectorizer()

X_train_counts = count_vect.fit_transform(text_documents)




#print(normalize_tweet(data[0]['text']))
