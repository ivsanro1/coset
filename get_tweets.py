#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re



# regular expressions used to clean up the tweet data
http_re = re.compile(r'http://[^\s]*|https://[^\s]*')
remove_ellipsis_re = re.compile(r'\.\.\.|…')
at_sign_re = re.compile(r'\@\S+')
punct_re = re.compile(r"[\"'\[\],.:;()\-&!]")
price_re = re.compile(r"\d+\.\d\d")
number_re = re.compile(r"\d+")

# converts to lower case and clean up the text
def normalize_tweet(tweet):
    t = tweet.lower()
    t = re.sub(http_re, ' LINK', t)
    t = re.sub(price_re, 'PRICE', t)
    t = re.sub(remove_ellipsis_re, '', t)
    t = re.sub(punct_re, '', t)
    t = re.sub(at_sign_re, '@', t)
    t = re.sub(number_re, 'NUM', t)
    return t


path_train = "train/coset-train.csv"

with open(path_train) as f:
    content = f.readlines()
# remove whitespace characters like '\n' at the end of each line
content = [x.strip() for x in content]

pattern_tweeetID = re.compile('"[0-9]{18}"')

tweets_text = []
tweet = ""

for line in content:
	if(pattern_tweeetID.match(line[:20])):
		#New tweet, append last
		tweets_text.append(tweet)
		tweet = line
	else:
		tweet += line

# "id","text","topic"
data = []

for tweet in tweets_text:
	tweet_id = tweet[1:19]
	if(re.compile('[0-9]{2}').match(tweet[-3:])): # if match is true, then the tag is 2 digits
		tweet_topic = tweet[-3:-1]
		tweet_text = tweet[22:-6]
	else:
		tweet_topic = tweet[-2:-1]
		tweet_text = tweet[22:-5]
	data.append({'id': tweet_id, 'text': tweet_text, 'topic' : tweet_topic})


print len(data)
