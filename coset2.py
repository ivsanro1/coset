#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import re
import csv
import nltk
from nltk.tokenize import RegexpTokenizer
import numpy as np




# regular expressions used to clean up the tweet data
http_re = re.compile(r'http://[^\s]*|https://[^\s]*')
remove_ellipsis_re = re.compile(r'\.\.\.|…')
at_sign_re = re.compile(r'\@\S+')
hashtag_re = re.compile(r'\#\S+')
punct_re = re.compile(r"[\"'\[\],.:;()\-&!]")
price_re = re.compile(r"\d+\.\d\d")
number_re = re.compile(r"\d+")
rt_re = re.compile(r"rt")



def normalize_tweet(tweet):
    t = tweet.lower()
    t = re.sub(http_re, 'ENLACE', t)
    t = re.sub('\n|\t', ' ', t)

    t = re.sub(''':|;|'|"|\?|¿|¡|!|$|%|/|\(|\)|\-|\.|,|º|…|#|''', '', t)
    t = re.sub('\s\d+\s', 'NUM', t)


    tokenizer = RegexpTokenizer('[a-záéíóúñ0-9_]+|[A-ZÑ]+')
    t = tokenizer.tokenize(t)
    t = ' '.join(t)

    return t

def tweet_features_bigrams(tweet):
    features = {}

    text = normalize_tweet(tweet['text'])

    for bigrams in nltk.bigrams(text.split(' ')):
        features['contains(%s)' % ','.join(bigrams)] = True

    return features

def tweet_features_unigrams(tweet):
    features = {}

    text = normalize_tweet(tweet['text'])

    for unigram in text.split(' '):
        features['contains(%s)' % unigram] = True

    return features




path_train = "train/coset-train.csv"
path_dev = "train/coset-dev.csv"
#path_test = "test/coset-text.csv"

num_folds = 10

data_train = []
data_dev = []
#data_test = []

with open(path_train, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_train.append({'id':row[0],'text':row[1],'topic':row[2]})

with open(path_dev, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_dev.append({'id':row[0],'text':row[1],'topic':row[2]})

#with open(path_test, 'r') as csvfile:
#    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
#    for row in tweetreader:
#        data_test.append({'id':row[0],'text':row[1]})

data_train.pop(0)
data_dev.pop(0)
#data_test.pop(0)

data = data_train+data_dev

subset_size = len(data)//num_folds

# get features and targets
features = [ (tweet_features_unigrams(d), d['topic']) for d in data ]

accuracies = [0]*num_folds



for i in range(num_folds):
    testing_this_round = features[i*subset_size:(i+1)*subset_size]
    training_this_round = features[:i*subset_size] + features[(i+1)*subset_size:]



    classifier = nltk.NaiveBayesClassifier.train(training_this_round)
    #classifier.show_most_informative_features(20)

    errors = []

    for unigrams, target in testing_this_round:
        guess = classifier.classify(unigrams)
        if guess != target:
            errors.append( (target, guess, d) )

    acc = (1-(len(errors)/subset_size))
    print 'fold #%i' % i
    print 'Total errors: %d' % len(errors)
    print 'Accuracy: %.3f\n' % acc

    accuracies[i] = acc

print '\nMean accuracy: %.3f' % np.mean(accuracies)
