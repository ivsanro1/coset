#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import re
import csv
import nltk
from nltk.tokenize import RegexpTokenizer
import numpy as np
from sklearn.metrics import confusion_matrix




# regular expressions used to clean up the tweet data
http_re = re.compile(r'http://[^\s]*|https://[^\s]*')
remove_ellipsis_re = re.compile(r'\.\.\.|…')
at_sign_re = re.compile(r'\@\S+')
hashtag_re = re.compile(r'\#\S+')
punct_re = re.compile(r"[\"'\[\],.:;()\-&!]")
price_re = re.compile(r"\d+\.\d\d")
number_re = re.compile(r"\d+")
rt_re = re.compile(r"rt")


# converts to lower case and clean up the text
def normalize_tweet2(tweet):
    t = tweet.lower()
    t = re.sub(http_re, 'LINK', t)
    t = re.sub(price_re, 'PRICE', t)
    t = re.sub(remove_ellipsis_re, '', t)
    t = re.sub(punct_re, '', t)
    t = re.sub(at_sign_re, '@', t)
    t = re.sub(number_re, 'NUM', t)
    return t

# converts to lower case and clean up the text
def normalize_tweet3(tweet):
    t = tweet.lower()
    t = re.sub(http_re, 'ENLACE', t)
    t = re.sub(price_re, 'NUMERO', t)
    t = re.sub(hashtag_re, 'HASHTAG', t)

    tokenizer = RegexpTokenizer(u'[a-záéíóúñ]+|[A-ZÑ]+')
    t = tokenizer.tokenize(t)
    t = ' '.join(t)

    t = re.sub(rt_re, '', t)

    return t

def normalize_tweet(tweet):
    t = tweet.lower()
    t = re.sub(http_re, 'ENLACE', t)
    t = re.sub('\n|\t', ' ', t)

    t = re.sub('á', 'a', t)
    t = re.sub('é', 'e', t)
    t = re.sub('í', 'i', t)
    t = re.sub('ó', 'o', t)
    t = re.sub('ú', 'u', t)
    t = re.sub(r'[^\x00-\x7F]+','', t)

    t = re.sub(''':|;|'|"|\?|¿|¡|!|$|%|/|\(|\)|\-|\.|,|º|…|#''', '', t)
    t = re.sub('\s\d+\s', 'NUM', t)

    t = re.sub('\s\s+', ' ', t)

    tokenizer = RegexpTokenizer('[a-z0-9_ñ]+|[A-ZÑ]+')
    t = tokenizer.tokenize(t)
    t = ' '.join(t)

    return t

def tweet_features_bigrams(tweet):
    features = {}

    text = normalize_tweet(tweet['text'])

    for bigrams in nltk.bigrams(text.split(' ')):
        features['contains(%s)' % ','.join(bigrams)] = True

    return features

def tweet_features_unigrams(tweet):
    features = {}

    text = normalize_tweet(tweet['text'])

    for unigram in text.split(' '):
        features['contains(%s)' % unigram] = True

    return features




path_train = "train/coset-train.csv"
path_dev = "train/coset-dev.csv"
#path_test = "test/coset-text.csv"

num_folds = 10

data_train = []
data_dev = []
#data_test = []

with open(path_train, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_train.append({'id':row[0],'text':row[1],'topic':row[2]})

with open(path_dev, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_dev.append({'id':row[0],'text':row[1],'topic':row[2]})

#with open(path_test, 'r') as csvfile:
#    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
#    for row in tweetreader:
#        data_test.append({'id':row[0],'text':row[1]})

data_train.pop(0)
data_dev.pop(0)
#data_test.pop(0)

data = data_train+data_dev

subset_size = len(data)//num_folds

# get features and targets
features = [ (tweet_features_unigrams(d), d['topic']) for d in data ]

accuracies = [0]*num_folds

confusion = []

for i in range(num_folds):
    testing_this_round = features[i*subset_size:(i+1)*subset_size]
    training_this_round = features[:i*subset_size] + features[(i+1)*subset_size:]



    classifier = nltk.NaiveBayesClassifier.train(training_this_round)
    #classifier.show_most_informative_features(20)

    errors = 0

    y_pred = []
    y_true = []

    for unigrams, target in testing_this_round:
        guess = classifier.classify(unigrams)
        y_pred.append(guess)
        y_true.append(target)
        if guess != target:
            errors += 1


    if(confusion == []):
        confusion = confusion_matrix(y_true, y_pred)
    else:
        confusion = confusion+confusion_matrix(y_true, y_pred)


    acc = (1-(errors/subset_size))
    print 'fold #%i' % i
    print 'Total errors: %d' % errors
    #print 'Accuracy: %.3f\n' % acc

    accuracies[i] = acc




print 'Confusion matrix:\n'
print confusion


classes = [1,2,9,10,11]




for i in xrange(5):
    precision = confusion[i,i]/sum(row[i] for row in confusion)
    recall = confusion[i,i]/np.sum(confusion[i])
    print '\nPrecision for class %i is: %.5f' % (classes[i], precision)
    print 'Recall for class %i is: %.5f' % (classes[i], recall)
    print 'F1 score for class %i is: %.5f' % (classes[i], 2*((precision*recall)/(precision+recall)))
