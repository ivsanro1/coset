#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import re
import csv
import nltk
from nltk.tokenize import RegexpTokenizer
import numpy as np




# regular expressions used to clean up the tweet data
http_re = re.compile(r'http://[^\s]*|https://[^\s]*')
remove_ellipsis_re = re.compile(r'\.\.\.|…')
at_sign_re = re.compile(r'\@\S+')
hashtag_re = re.compile(r'\#\S+')
punct_re = re.compile(r"[\"'\[\],.:;()\-&!]")
price_re = re.compile(r"\d+\.\d\d")
number_re = re.compile(r"\d+")
rt_re = re.compile(r"rt")


# converts to lower case and clean up the text
def normalize_tweet2(tweet):
    t = tweet.lower()
    t = re.sub(http_re, 'LINK', t)
    t = re.sub(price_re, 'PRICE', t)
    t = re.sub(remove_ellipsis_re, '', t)
    t = re.sub(punct_re, '', t)
    t = re.sub(at_sign_re, '@', t)
    t = re.sub(number_re, 'NUM', t)
    return t

# converts to lower case and clean up the text
def normalize_tweet(tweet):
    t = tweet.lower()
    t = re.sub(http_re, 'ENLACE', t)
    t = re.sub(price_re, 'NUMERO', t)
    t = re.sub(hashtag_re, 'HASHTAG', t)

    tokenizer = RegexpTokenizer(u'[a-záéíóúñ]+|[A-ZÑ]+')
    t = tokenizer.tokenize(t)
    t = ' '.join(t)

    t = re.sub(rt_re, '', t)

    return t

def tweet_features_bigrams(tweet):
    features = {}

    text = normalize_tweet(tweet['text'])

    for bigrams in nltk.bigrams(text.split(' ')):
        features['contains(%s)' % ','.join(bigrams)] = True

    return features

def tweet_features_unigrams(tweet):
    features = {}

    text = normalize_tweet(tweet['text'])

    for unigram in text.split(' '):
        features['contains(%s)' % unigram] = True

    return features




path_train = "train/coset-train.csv"
path_dev = "train/coset-dev.csv"
#path_test = "test/coset-text.csv"

num_folds = 10

data_train = []
data_dev = []
#data_test = []

with open(path_train, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_train.append({'id':row[0],'text':row[1],'topic':row[2]})

with open(path_dev, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_dev.append({'id':row[0],'text':row[1],'topic':row[2]})

#with open(path_test, 'r') as csvfile:
#    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
#    for row in tweetreader:
#        data_test.append({'id':row[0],'text':row[1]})

data_train.pop(0)
data_dev.pop(0)
#data_test.pop(0)

data = data_train+data_dev



frase = data[0]['text']
result = frase.lower()
result = re.sub(http_re, 'ENLACE', result)
result = re.sub('\n|\t', ' ', result)

result = re.sub('á', 'a', result)
result = re.sub('é', 'e', result)
result = re.sub('í', 'i', result)
result = re.sub('ó', 'o', result)
result = re.sub('ú', 'u', result)

result = re.sub(''':|;|'|"|\?|¿|¡|!|$|%|/|\(|\)|\-|\.|,|º|…|#''', '', result)
result = re.sub('\s\d+\s', 'NUM', result)


tokenizer = RegexpTokenizer('[a-z0-9_ñ]+|[A-ZÑ]+')
result = tokenizer.tokenize(result)
result = ' '.join(result)

#result = re.sub('[^a-zA-Z\d\. ]|( ){2,}','',frase )

print frase
print '\n'
print result
