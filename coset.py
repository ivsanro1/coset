#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import re
import csv
import nltk
from nltk.tokenize import RegexpTokenizer




# regular expressions used to clean up the tweet data
http_re = re.compile(r'http://[^\s]*|https://[^\s]*')
remove_ellipsis_re = re.compile(r'\.\.\.|…')
at_sign_re = re.compile(r'\@\S+')
punct_re = re.compile(r"[\"'\[\],.:;()\-&!]")
price_re = re.compile(r"\d+\.\d\d")
number_re = re.compile(r"\d+")

# converts to lower case and clean up the text
def normalize_tweet2(tweet):
    t = tweet.lower()
    t = re.sub(http_re, ' LINK', t)
    t = re.sub(price_re, 'PRICE', t)
    t = re.sub(remove_ellipsis_re, '', t)
    t = re.sub(punct_re, '', t)
    t = re.sub(at_sign_re, '@', t)
    t = re.sub(number_re, 'NUM', t)
    return t

# converts to lower case and clean up the text
def normalize_tweet(tweet):
    t = tweet.lower()
    t = re.sub(http_re, ' LINK', t)
    tokenizer = RegexpTokenizer(r'[a-záéíóú]+|[A-Z]+')
    t = tokenizer.tokenize(t)
    t = ' '.join(t)

    return t

def tweet_features(tweet):
    features = {}

    text = normalize_tweet(tweet['text'])

    for bigrams in nltk.bigrams(text.split(' ')):
        features['contains(%s)' % ','.join(bigrams)] = True

    return features




path_train = "train/coset-train.csv"
path_dev = "train/coset-dev.csv"
path_test = "test/coset-text.csv"

data_train = []
data_dev = []
data_test = []

with open(path_train, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_train.append({'id':row[0],'text':row[1],'topic':row[2]})

with open(path_dev, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_dev.append({'id':row[0],'text':row[1],'topic':row[2]})

with open(path_test, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_test.append({'id':row[0],'text':row[1]})

data_train.pop(0)
data_dev.pop(0)
data_test.pop(0)

train_set = [ (tweet_features(d), d['topic']) for d in data_train ]
dev_set = [ (tweet_features(d), d['topic']) for d in data_dev ]


classifier = nltk.NaiveBayesClassifier
classifier = nltk.NaiveBayesClassifier.train(train_set)
#classifier.show_most_informative_features(20)

errors = []

for d in data_dev:
    label = d['topic']
    guess = classifier.classify(tweet_features(d))
    if guess != label:
        errors.append( (label, guess, d) )

print 'Total errors: %d' % len(errors)



print 'Accuracy: %.3f' % (1-(len(errors)/len(dev_set)))

print normalize_tweet(data_train[8]['text'])
