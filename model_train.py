#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import re
import csv
import nltk
from nltk.tokenize import RegexpTokenizer
import numpy as np
from sklearn.metrics import confusion_matrix
import argparse
import os
import cPickle
from time import time




# regular expressions used to clean up the tweet data
http_re = re.compile(r'http://[^\s]*|https://[^\s]*')
remove_ellipsis_re = re.compile(r'\.\.\.|…')
at_sign_re = re.compile(r'\@\S+')
hashtag_re = re.compile(r'\#\S+')
punct_re = re.compile(r"[\"'\[\],.:;()\-&!]")
price_re = re.compile(r"\d+\.\d\d")
number_re = re.compile(r"\d+")
rt_re = re.compile(r"rt")



def normalize_tweet(tweet):
    t = tweet.lower()
    t = re.sub(http_re, 'ENLACE', t)
    t = re.sub('\n|\t', ' ', t)

    t = re.sub('á', 'a', t)
    t = re.sub('é', 'e', t)
    t = re.sub('í', 'i', t)
    t = re.sub('ó', 'o', t)
    t = re.sub('ú', 'u', t)
    t = re.sub(r'[^\x00-\x7F]+','', t)

    t = re.sub(''':|;|'|"|\?|¿|¡|!|$|%|/|\(|\)|\-|\.|,|º|…|#''', '', t)
    t = re.sub('\s\d+\s', 'NUM', t)

    t = re.sub('\s\s+', ' ', t)

    tokenizer = RegexpTokenizer('[a-z0-9_ñ]+|[A-ZÑ]+')
    t = tokenizer.tokenize(t)
    t = ' '.join(t)

    return t

def tweet_features_bigrams(tweet):
    features = {}

    text = normalize_tweet(tweet['text'])

    for bigrams in nltk.bigrams(text.split(' ')):
        features['contains(%s)' % ','.join(bigrams)] = True

    return features

def tweet_features_unigrams(tweet):
    features = {}

    text = normalize_tweet(tweet['text'])

    for unigram in text.split(' '):
        features['contains(%s)' % unigram] = True

    return features


def parse_args():
    parser = argparse.ArgumentParser(description='Trains the NB classifier for the COSET 2017 task, given the training and development set in csv format. The training is made using both the training and development data sets. This model consists in cleaning the tweets, get unigrams and train a Naive Bayes Classifier to classify tweets into topics. The classifier is saved into a pickle object that can be loaded later (i.e., in predict.py).                                                       Example of use: python model_train.py train/coset-train.csv train/coset-dev.csv model/nb_classifier.pkl')
    parser.add_argument('path_train', metavar='train',
                        help='relative path to the training data set. Example: train/coset-train.csv ')
    parser.add_argument('path_dev', metavar='dev',
                        help='relative path to development data set. Example: train/coset-dev.csv')
    parser.add_argument('path_classifier', metavar='out',
                        help='relative path to the resulting classifier (output). Example: model/nb_classifier.pkl')

    return parser.parse_args()


args = parse_args()




path_train = args.path_train
path_dev = args.path_dev
path_output = args.path_classifier


data_train = []
data_dev = []

with open(path_train, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_train.append({'id':row[0],'text':row[1],'topic':row[2]})

with open(path_dev, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_dev.append({'id':row[0],'text':row[1],'topic':row[2]})

#with open(path_test, 'r') as csvfile:
#    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
#    for row in tweetreader:
#        data_test.append({'id':row[0],'text':row[1]})

data_train.pop(0)
data_dev.pop(0)

data = data_train+data_dev

training_time = 0

print ('\nObtaining data features ...')
t0 = time()
# get features and targets
features = [ (tweet_features_unigrams(d), d['topic']) for d in data ]
time_elapsed = (time() - t0)
print("... done in %0.3fs" % time_elapsed)
training_time += time_elapsed

print ('Training Naive Bayes Classifier... Data set size: %i' % len(data))
t0 = time()

classifier = nltk.NaiveBayesClassifier.train(features)
time_elapsed = (time() - t0)
print("... done in %0.3fs" % time_elapsed)
training_time += time_elapsed

print ('\nNB classifier trained with %i tweets. Total training time: %.3f (%i tweets/s)\n' % (len(data), training_time, len(data)//training_time))




if not os.path.exists(path_output.split('/')[0]):
    os.makedirs(path_output.split('/')[0])

print ("Saving Naive Bayes Classifier to path '%s'" % path_output)
t0 = time()
with open(path_output, 'wb') as fid:
    cPickle.dump(classifier, fid)
print("... done in %0.3fs" % (time() - t0))
