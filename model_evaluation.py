#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import re
import csv
import nltk
from nltk.tokenize import RegexpTokenizer
import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
import argparse




# regular expressions used to clean up the tweet data
http_re = re.compile(r'http://[^\s]*|https://[^\s]*')
remove_ellipsis_re = re.compile(r'\.\.\.|…')
at_sign_re = re.compile(r'\@\S+')
hashtag_re = re.compile(r'\#\S+')
punct_re = re.compile(r"[\"'\[\],.:;()\-&!]")
price_re = re.compile(r"\d+\.\d\d")
number_re = re.compile(r"\d+")
rt_re = re.compile(r"rt")



def normalize_tweet(tweet):
    t = tweet.lower()
    t = re.sub(http_re, 'ENLACE', t)
    t = re.sub('\n|\t', ' ', t)

    t = re.sub('á', 'a', t)
    t = re.sub('é', 'e', t)
    t = re.sub('í', 'i', t)
    t = re.sub('ó', 'o', t)
    t = re.sub('ú', 'u', t)
    t = re.sub(r'[^\x00-\x7F]+','', t)

    t = re.sub(''':|;|'|"|\?|¿|¡|!|$|%|/|\(|\)|\-|\.|,|º|…|#''', '', t)
    t = re.sub('\s\d+\s', 'NUM', t)

    t = re.sub('\s\s+', ' ', t)

    tokenizer = RegexpTokenizer('[a-z0-9_ñ]+|[A-ZÑ]+')
    t = tokenizer.tokenize(t)
    t = ' '.join(t)

    return t

def tweet_features_bigrams(tweet):
    features = {}

    text = normalize_tweet(tweet['text'])

    for bigrams in nltk.bigrams(text.split(' ')):
        features['contains(%s)' % ','.join(bigrams)] = True

    return features

def tweet_features_unigrams(tweet):
    features = {}

    text = normalize_tweet(tweet['text'])

    for unigram in text.split(' '):
        features['contains(%s)' % unigram] = True

    return features


def parse_args():
    parser = argparse.ArgumentParser(description='Evaluates the implemented model, given the training and development set. The evaluation is performed with a K=10 Cross Fold Validation. \n\nExample of use:\n python model_evaluation.py train/coset-train.csv train/coset-dev.csv')
    parser.add_argument('path_train', metavar='train',
                        help='relative path to the training data set. \nExample: train/coset-train.csv ')
    parser.add_argument('path_dev', metavar='dev',
                        help='relative path to development data set. \nExample: train/coset-dev.csv')




    return parser.parse_args()

args = parse_args()


path_train = args.path_train
path_dev = args.path_dev
#path_test = "test/coset-text.csv"

num_folds = 10

data_train = []
data_dev = []
#data_test = []

with open(path_train, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_train.append({'id':row[0],'text':row[1],'topic':row[2]})

with open(path_dev, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        data_dev.append({'id':row[0],'text':row[1],'topic':row[2]})

#with open(path_test, 'r') as csvfile:
#    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
#    for row in tweetreader:
#        data_test.append({'id':row[0],'text':row[1]})

data_train.pop(0)
data_dev.pop(0)
#data_test.pop(0)

data = data_train+data_dev

subset_size = len(data)//num_folds

# get features and targets
features = [ (tweet_features_unigrams(d), d['topic']) for d in data ]

accuracies = [0]*num_folds

confusion = []

for i in range(num_folds):
    testing_this_round = features[i*subset_size:(i+1)*subset_size]
    training_this_round = features[:i*subset_size] + features[(i+1)*subset_size:]



    classifier = nltk.NaiveBayesClassifier.train(training_this_round)
    #classifier.show_most_informative_features(20)

    errors = 0

    y_pred = []
    y_true = []

    for unigrams, target in testing_this_round:
        guess = classifier.classify(unigrams)
        y_pred.append(guess)
        y_true.append(target)
        if guess != target:
            errors += 1


    if(confusion == []):
        confusion = confusion_matrix(y_true, y_pred)
    else:
        confusion = confusion+confusion_matrix(y_true, y_pred)


    acc = (1-(errors/subset_size))
    print 'fold #%i' % i
    print 'Total errors: %d' % errors
    #print 'Accuracy: %.3f\n' % acc

    accuracies[i] = acc




print '\n\nConfusion matrix:\n'
print confusion


classes = [1,2,9,10,11]
precisions = []
recalls = []
f1s = []



for i in xrange(5):
    precision = confusion[i,i]/sum(row[i] for row in confusion)
    precisions.append(precision)
    recall = confusion[i,i]/np.sum(confusion[i])
    recalls.append(recall)
    f1 = 2*((precision*recall)/(precision+recall))
    f1s.append(f1)
    print '\nPrecision for class %i is: %.4f' % (classes[i], precision)
    print 'Recall for class %i is: %.4f' % (classes[i], recall)
    print 'F1 score for class %i is: %.4f' % (classes[i], f1)



print '\n\n\nMacro precision: %.4f' % (sum(precisions)/len(classes))
print 'Macro recall: %.4f' % (sum(recalls)/len(classes))
print 'Macro F1: %.4f' % (sum(f1s)/len(classes))
