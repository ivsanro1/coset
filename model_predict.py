#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import re
import csv
import nltk
from nltk.tokenize import RegexpTokenizer
import numpy as np
from sklearn.metrics import confusion_matrix
import argparse
import os
import cPickle
from time import time




# regular expressions used to clean up the tweet data
http_re = re.compile(r'http://[^\s]*|https://[^\s]*')
remove_ellipsis_re = re.compile(r'\.\.\.|…')
at_sign_re = re.compile(r'\@\S+')
hashtag_re = re.compile(r'\#\S+')
punct_re = re.compile(r"[\"'\[\],.:;()\-&!]")
price_re = re.compile(r"\d+\.\d\d")
number_re = re.compile(r"\d+")
rt_re = re.compile(r"rt")



def normalize_tweet(tweet):
    t = tweet.lower()
    t = re.sub(http_re, 'ENLACE', t)
    t = re.sub('\n|\t', ' ', t)

    t = re.sub('á', 'a', t)
    t = re.sub('é', 'e', t)
    t = re.sub('í', 'i', t)
    t = re.sub('ó', 'o', t)
    t = re.sub('ú', 'u', t)
    t = re.sub(r'[^\x00-\x7F]+','', t)

    t = re.sub(''':|;|'|"|\?|¿|¡|!|$|%|/|\(|\)|\-|\.|,|º|…|#''', '', t)
    t = re.sub('\s\d+\s', 'NUM', t)

    t = re.sub('\s\s+', ' ', t)

    tokenizer = RegexpTokenizer('[a-z0-9_ñ]+|[A-ZÑ]+')
    t = tokenizer.tokenize(t)
    t = ' '.join(t)

    return t

def tweet_features_bigrams(tweet):
    features = {}

    text = normalize_tweet(tweet['text'])

    for bigrams in nltk.bigrams(text.split(' ')):
        features['contains(%s)' % ','.join(bigrams)] = True

    return features

def tweet_features_unigrams(tweet):
    features = {}

    text = normalize_tweet(tweet['text'])

    for unigram in text.split(' '):
        features['contains(%s)' % unigram] = True

    return features


def parse_args():
    parser = argparse.ArgumentParser(description='Loads the NB classifier from the pickle object and performs a prediction of the topics of a test file. Example of use: python model_predict.py test/coset-text.csv model/nb_classifier.pkl output.txt')
    parser.add_argument('path_tweets', metavar='tweets.csv',
                        help='relative path to the file with the tweets to predict their topic. Example: test/coset-text.csv')
    parser.add_argument('path_classifier', metavar='classifier',
                        help='relative path to the classifier trained with model_train.py. Example: model/nb_classifier.pkl')
    parser.add_argument('path_prediction', metavar='prediction',
                        help='relative path to the file with the predictions. Example: output.txt')

    return parser.parse_args()


args = parse_args()


path_tweets = args.path_tweets
path_classifier = args.path_classifier
path_prediction = args.path_prediction

print("Loading model from %s..." % path_classifier)
t0 = time()
with open(path_classifier, 'rb') as fid:
    classifier = cPickle.load(fid)
print("... done in %0.3f" % (time() - t0))

dataToPredict = []


# csv file
with open(path_tweets, 'r') as csvfile:
    tweetreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in tweetreader:
        dataToPredict.append({'id':row[0],'text':row[1]})

dataToPredict.pop(0)


y_pred = []


t0 = time()
features = [ (tweet_features_unigrams(d)) for d in dataToPredict ]

for unigrams in features:
    guess = classifier.classify(unigrams)
    y_pred.append(guess)

f = open(path_prediction, 'w')

for i in range(len(dataToPredict)):
    string = dataToPredict[i]['id'] +'\t'+ y_pred[i] +'\n'
    f.write(string.encode('utf8'))


f.close()
